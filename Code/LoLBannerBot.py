import discord
from discord.ext import commands, tasks

client = commands.Bot(command_prefix=".", intents=discord.Intents.all())

'''
NOTE: In theory this should work, thank god no one plays league in my servers so i wasn't able to test it, if you find bugs
please submit them to my discord (not_forsale)
'''

@client.event
async def on_ready():
    print("Bot is ready for use\n-----------------------------")

@tasks.loop(minutes=3)
async def banlol():
     for guild in client.guilds:
          for member in guild.members:
               if member.bot:
                    continue
               if member.activity and member.activity.type == discord.ActivityType.playing:
                    if "League of Legends" in member.activity.name and member.activity.timestamps:
                        elapsedtime = (discord.utils.utcnow() - member.activity.timestamps.start).total_seconds()
                        if elapsedtime >= 900:
                            await member.send(f"You were banned from {guild.name} for playing league for more than 15 minutes")
                            await member.ban(reason="Playing league for more than 15 minutes")
                        elif elapsedtime >= 600:
                             await member.send(f"Warning: Stop playing league, consequences WILL follow.")

client.run('Insert your token here') #INSERT YOUR TOKEN
